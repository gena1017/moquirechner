/**
 * View Models used by Spring MVC REST controllers.
 */
package com.jhipster.moquirechner.web.rest.vm;
